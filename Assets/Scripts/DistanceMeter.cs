﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceMeter : MonoBehaviour {

    Text guitexty;

    public GameObject Player;

    float beginPos;
    float curPos;

    public int multiplier = 10;

	// Use this for initialization
	void Start () 
    {
        beginPos = Player.transform.position.x;
        guitexty = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
    {

        curPos = Player.transform.position.x - beginPos;

        int distance = Mathf.Abs(Mathf.RoundToInt(curPos * multiplier));

        if (distance > PlayerPrefs.GetInt("HighScore"))
            PlayerPrefs.SetInt("HighScore", distance);

        guitexty.text= "Distance: " + distance.ToString() + "m /" + PlayerPrefs.GetInt("HighScore") + "m"; 

	}
}
