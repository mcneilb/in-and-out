﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shapes : MonoBehaviour {

    public GameObject obj;
    public Transform generationpoint;
    public float distanceBetween;

    private float platformWidth;

    void Start()
    {
        platformWidth = obj.GetComponent<BoxCollider2D>().size.x;
    }
    void Update()
    {
        if (transform.position.x < generationpoint.position.x)
        {
            transform.position = new Vector4(transform.position.x + platformWidth + distanceBetween, transform.position.y,transform.position.z);

            Instantiate(obj, transform.position, transform.rotation);
        }
    }
}
