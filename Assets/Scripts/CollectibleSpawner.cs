﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleSpawner : MonoBehaviour {

    public GameObject obj;
    public Transform generationpoint;
    public float distanceBetween;

    private float platformWidth;

    void Start()
    {
        platformWidth = obj.GetComponent<CircleCollider2D>().radius;
    }
    void Update()
    {
        if (transform.position.x < generationpoint.position.x)
        {
            transform.position = new Vector3(transform.position.x + platformWidth + distanceBetween, transform.position.y, transform.position.z);

            Instantiate(obj, transform.position, transform.rotation);
        }
    }
}
